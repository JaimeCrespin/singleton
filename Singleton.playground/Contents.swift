import UIKit
class SteamManager {
  
    static var shared: SteamManager = {
        let profile = SteamManager()
        return profile
    }()
    
    private var profileName: String
    
    private init(){
        self.profileName = ""
    }
    func setSteamID(name: String){
        self.profileName = name
    }

    func getSteamID(){
        print("Your profile is: httpps://steamcommunity.com/id/\(profileName)")
    }
    
}

func callSteamProfile(){
    let user1 = SteamManager.shared
    let user2 = SteamManager.shared
    user1.setSteamID(name: "MrJames")
    user1.getSteamID()
    user2.setSteamID(name: "NiRou")
    user2.getSteamID()
    user2.getSteamID()
}
callSteamProfile()
